# README #

This repository contains the source code of the method called semi-supervised online kernel matrix 
factorization (SS-OKMF).

SS-OKMF performs a semantic embedding by finding a non-linear mapping to a low-dimensional 
semantic space modeled   by the original high-dimensional feature representation and the class representation. 
An important characteristic of the SS-OKMF is that the new low-dimensional semantic representation can be learned 
in a semi-supervised fashion. Therefore, the annotated instances can be used to maximize the discrimination 
between classes, but also, the non-annotated instances can be exploited to estimate the intrinsic manifold 
structure of the data. The non-linear modeling is based on kernel methods with a learning in a budget 
strategy that allows keeping low memory requirements. This strategy along with an online formulation based 
on stochastic gradient descent reduces the computation time and keeps low computational requirements in 
large-scale problems. According to the experimental evaluation performed, on several datasets of different 
nature (i.e. images, biosignals and synthetic data), the proposed SS-OKMF, in comparison with several 
non-linear supervised, unsupervised and semi-supervised dimensionality reduction methods, presents a 
competitive performance in classification tasks under a transductive learning setup preserving a lower 
computational cost. 

##Keywords: 
Semantic Representation, Semi-Supervised Learning, Transductive Learning,Learning on a Budget, 
Multi-class Classification