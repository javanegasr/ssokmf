"""
.. module:: ssokmf_keras
   :synopsis: Custom layer definitions for SSOKMF

.. moduleauthor:: Jorge A. Vanegas <javanegasr@unal.edu.co>
"""

from keras import backend as K
from keras.engine.topology import Layer
from keras import initializations
from keras import regularizers

from kernels import kernel

class KernelProjection(Layer):
    """
    Definitions for kernel projection layer
    """   
 
    def __init__(self, embed_dim, init='uniform', W_regularizer=None, 
                 budget=None, metric='linear', params={}, **kwargs):
        self.embed_dim = embed_dim
        self.init = initializations.get(init)
        self.W_regularizer = regularizers.get(W_regularizer)
        self.budget = None
        if budget is not None:
            self.budget = K.variable(value=budget)
            self.budget_size = budget.shape[0]
        self.metric = metric
        self.params = params

        super(KernelProjection, self).__init__(**kwargs)

    def build(self, input_shape):
        # model parameters definition and initialization  
        if self.metric == 'precomputed':
	    self.budget_size = input_shape[1]
      
        self.Wx = self.add_weight(shape=(self.embed_dim, self.budget_size),
                                  initializer=self.init,
                                  trainable=True,
                                  name='Wx',                               
                                  regularizer=self.W_regularizer)
        super(KernelProjection, self).build(input_shape)

    def call(self, x, mask=None):
        Kxb = kernel(x, y=self.budget, metric=self.metric, **self.params) 
        return K.transpose(K.dot(self.Wx, K.transpose(Kxb)))
                   
    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.embed_dim)
    
    
class KernelBackProjection(Layer):
    """
    Definitions for kernel backprojection layer
    """

    def __init__(self, budget_dim, init='uniform', 
                 W_regularizer=None, **kwargs):
        self.budget_dim = budget_dim
        self.init = initializations.get(init)
        self.W_regularizer = regularizers.get(W_regularizer)
        super(KernelBackProjection, self).__init__(**kwargs)

    def build(self, input_shape):
        # model parameters definition and initialization  
        self.Wxp = self.add_weight(shape=(self.budget_dim, input_shape[1]),
                                  initializer=self.init,
                                  trainable=True,
                                  name='Wxp',
                                  regularizer=self.W_regularizer)
        super(KernelBackProjection, self).build(input_shape)

    def call(self, h, mask=None):
        return K.transpose(K.dot(self.Wxp, K.transpose(h)))
                   
    def get_output_shape_for(self, input_shape):
        return (input_shape[0], self.budget_dim)
    

