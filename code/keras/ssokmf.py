from keras.models import Sequential, Model
from keras.layers import Input, Dense, merge, Embedding, normalization, Dropout
from keras.regularizers import l2
from keras import backend as K
from keras.engine.topology import Layer
from keras import regularizers

import numpy as np
from sklearn.metrics.pairwise import pairwise_kernels

from layers import KernelProjection, KernelBackProjection 
import losses as ls


class OKMF():
    """
    Semi-supervised Online Kernel Matrix Factorization Algortihm
    implementation in Keras 
    """

    # allowed parameters with default values
    params = {
        'topics': 10,
        'minibatch': 256,
        'epochs': 10,
        'budget': None,
        'kernel': {'metric': 'linear'},
        'learning': {'rule':'rmsprop'},
        'x': {'loss': 'mean_squared_error', 'alpha': 1},
        'y': {'loss': 'mean_squared_error', 'beta': 1, 'activation': 'linear'},
        'Wx': {'init': 'uniform', 'lambda': 0.001},
        'Wxp': {'init': 'uniform', 'lambda': 0.001},
        'Wy': {'init': 'uniform', 'lambda': 0.001}
    }

    def __init__(self, X, Y=None, B=None, **params):

        # read parameters
        for key, value in params.iteritems():
            if key in self.params:
                self.params[key] = value
            else:
                print('Unrecognized parameter \'{}\''.format(key))
        
        # define budget if not given
        if B is None:
            if self.params['budget'] is None:
                B = X
            else:
                B = self._make_budget(X)

        # define SSOKMF model
        self._make_model(X, Y, B, self.params['topics'])
        self._compile()

    def _make_budget(self, X):
        # make random budget
        idx = np.arange(X.shape[0])
        np.random.shuffle(idx)
        return X[idx[:self.params['budget']],:]


    def _make_model(self, X, Y, B, embed_dim):
        """
        Main model definition
        """

        self.X = X


        if Y is not None:
           # semisupervised, indicator variable
           self.I = (((Y!=0).sum(axis=1)!=0)*1)[np.newaxis].T
           Y = np.concatenate((self.I, Y), axis=1)
        self.Y = Y       
 
        self.B = B
        kernel_params = self.params['kernel'].copy()
        del kernel_params['metric']

        kernel_input = Input(shape=(X.shape[1],), 
                             dtype='float32', 
                             name='main_input')

        shared_embedding0 = KernelProjection(embed_dim,
                                            budget=B, 
                                            W_regularizer=l2(self.params['Wx']['lambda']),
                                            metric=self.params['kernel']['metric'],
                                            params=kernel_params,
                                            init=self.params['Wx']['init'],
                                            name='embedding')(kernel_input)             
        
        shared_embedding = shared_embedding0 
        #shared_embedding2 = normalization.BatchNormalization()(shared_embedding)
        #shared_embedding2 = Dropout(0.5)(shared_embedding)
      
        # y reconstruction layer
        #s(Wx*h)
        kxb_output = KernelBackProjection(B.shape[0], 
                                          W_regularizer=l2(self.params['Wxp']['lambda']), 
                                          init=self.params['Wxp']['init'],
                                          name="kxb_output")(shared_embedding)
        
        if Y is not None:
           y_output = Dense(Y.shape[1], 
                            activation=self.params['y']['activation'], 
                            init=self.params['Wy']['init'],
                            W_regularizer=l2(self.params['Wy']['lambda']),
                            name="y_output")(shared_embedding)

           self.model = Model(input=[kernel_input], 
                              output=[kxb_output, y_output])
        else:
           self.model = Model(input=[kernel_input],
                              output=[kxb_output])

        return self.model

    def _compile(self):
        if self.params['y']['loss'] == 'binary_crossentropy':
            self.params['y']['loss'] = ls.ss_binary_crossentropy
        if self.params['y']['loss'] == 'categorical_crossentropy':
            self.params['y']['loss'] = ls.ss_categorical_crossentropy
        if self.params['y']['loss'] == 'mean_squared_error':
            self.params['y']['loss'] = ls.ss_mean_squared_error

        kernel_params = self.params['kernel'].copy()
        del kernel_params['metric']
        self.KBB = pairwise_kernels(self.B, 
                                    metric=self.params['kernel']['metric'], 
                                    **kernel_params)

        if self.Y is None:
            losses = {'kxb_output': ls.k_mean_squared_error(self.KBB)}
            weights = {'kxb_output': 1}
        else:
            losses = {'kxb_output': ls.k_mean_squared_error(self.KBB),
                      'y_output': self.params['y']['loss']}
            weights = {'kxb_output': self.params['x']['alpha'],
                       'y_output': self.params['y']['beta']}

        self.model.compile(optimizer=self.params['learning']['rule'],
                           loss=losses,
                           loss_weights=weights)

    def fit(self, validation_split=0.2, verbose=1):
        kernel_params = self.params['kernel'].copy()
        del kernel_params['metric']

        self.KXB = pairwise_kernels(self.X, 
                                    self.B, 
                                    metric=self.params['kernel']['metric'], 
                                    **kernel_params) 

        if self.Y is None:
            outputs = {'kxb_output': self.KXB}
        else:
            outputs = {'kxb_output': self.KXB, 'y_output': self.Y}

        return self.model.fit({'main_input': self.X},
                              outputs,
                              nb_epoch=self.params['epochs'], 
                              batch_size=self.params['minibatch'], 
                              validation_split=validation_split,
                              verbose=verbose)

    def predict(self, x):
        return self.model.predict([x],
                                  batch_size=self.params['minibatch'],
                                  verbose=0)

    def predict_y(self, x):
        return self.predict(x)[1][:,1:]
  
    def predict_h(self, x):
        get_shared_embedding = K.function([self.model.get_layer('main_input').input],
                                          [self.model.get_layer('embedding').output])
        return get_shared_embedding([x])[0]
        
