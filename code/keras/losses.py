from keras import backend as K
from kernels import kernel

def ss_binary_crossentropy(y_true, y_pred):
   i = y_true[:,0]
   s = K.sum(i)
   n = K.switch(K.equal(0.0, s), K.variable(1), s)
   return K.sum(K.mean(K.binary_crossentropy(y_pred[:,1:], y_true[:,1:]), axis=1)*i)/n

def ss_categorical_crossentropy(y_true, y_pred):
    # TODO check aggregation
    return K.categorical_crossentropy(y_pred, y_true)

def ss_mean_squared_error(y_true, y_pred):
    i = y_true[:,0]
    return K.sum(K.square(y_pred, y_true[:,1:])*i)/K.sum()

def k_mean_squared_error(Kbb):
    KBB = K.variable(value=Kbb) 
    def kmse(y_true, y_pred):
        # y_true k(x,B)
        # y_pred = k(x,B)*Wx*Wxp
 
        return 1 - 2 * K.mean(K.dot(y_true, K.transpose(y_pred))) \
               + K.mean(K.dot(y_pred, K.dot(KBB, K.transpose(y_pred))))

    return kmse
