"""
.. module:: ssokmf_keras
   :synopsis: Kernel definitions for Keras backend

.. moduleauthor:: Jorge A. Vanegas <javanegasr@unal.edu.co>
"""

from keras import backend as K
from keras.engine.topology import Layer

# K(x, y) = exp(-gamma ||x-y||^2)
def rbf_kernel(x, y, gamma=1):

    print 'gamma', gamma
    x2 = K.permute_dimensions(K.sum(x**2, axis=1), ('x', 0))
    y2 = K.sum(y**2, axis=1)
    K0 = K.transpose(x2) - 2*K.dot(x, K.transpose(y)) + y2
    Kxy = K.exp(-gamma*K0)
    #return K.l2_normalize(Kxy, axis=1)
    return Kxy

# K(x, y) = x*y
def linear_kernel(x, y):
    return K.dot(x, K.transpose(y))

# K(x, y) = x*y/(||x||||y||)
def cosine_kernel(x, y):
    return K.dot(x, K.transpose(y)) / (K.reshape(K.sum(K.sqrt(x**2), axis=1), (-1, 1)) * K.sum(K.sqrt(y**2), axis=1))
    
def kernel(x, y=None, metric='linear', **kwargs):
    
    if y is None:
        y = x
    if metric == 'linear':
        return linear_kernel(x, y)
    if metric == 'cosine':
        return cosine_kernel(x, y)
    if metric == 'rbf':
        return rbf_kernel(x, y, gamma=kwargs['gamma'])
    if metric == 'precomputed':
        return x

class Kernel(Layer):
    
    def __init__(self, budget=None, metric='linear', params={}, **kwargs):
        self.budget = K.variable(value=budget)
        self.budget_size = None if metric == 'precomputed' else budget.shape[0]
        self.metric = metric
        self.params = params
        super(Kernel, self).__init__(**kwargs)

    def build(self, input_shape):
        super(Kernel, self).build(input_shape)

    def call(self, x, mask=None):
        return kernel(x, y=self.budget, metric=self.metric, **self.params)
                   
    def get_output_shape_for(self, input_shape):
        if self.budget_size is None:
            return input_shape
        else:
            return (None, self.budget_size)

