import numpy as np

from keras.models import Sequential, Model
from keras.layers import Input

from sklearn.metrics.pairwise import pairwise_kernels

from kernels import Kernel

x1 = np.array([[40, .2, 10, 30, 0.1],
               [ 3,  4,  4,  7,  20], 
               [10, 20,  5, .6,  1], 
               [7,   8,  4, 10, 30],
               [2.1, 7, 20, 15, 40]])
x2 = np.array([[1,  .8,  1, .8,  5],
               [50,  2,  2, .4, 20], 
               [20,  41, 1, .8, 0.2]])


x1 = np.random.rand(30,20)
x2 = np.random.rand(10,20)

metric = 'rbf'
gamma = 1

Kxy_skl = pairwise_kernels(x1, x2, metric=metric, gamma=gamma)
print 'sklearn:', Kxy_skl


input1 = Input(shape=(x1.shape[1],), dtype='float32', name='input1')
output = Kernel(name="output", budget=x2, metric=metric, params={'gamma':gamma})(input1)
model = Model(input=[input1], output=[output])

model.compile(loss='mean_squared_error', optimizer='sgd')

hist =  model.fit({'input1': x1},
                  {'output':Kxy_skl},
                   nb_epoch=1, 
                   batch_size=4)

Kxy_k = model.predict([x1], batch_size=4, verbose=0)
print 'keras:', Kxy_k


print ((Kxy_k - Kxy_skl) > 1e-6)*1
