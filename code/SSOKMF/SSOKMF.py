# -*- coding: utf-8 -*-
"""
@author: Jorge A. Vanegas 
"""

import numpy as np
import numpy.linalg as la
from numpy.random import permutation
from numpy import dot
from sklearn.metrics import pairwise_kernels as K
from KKmeans import KKmeans as kk
from L1Normalization import L1Normalization
from time import time

class SSOKMF:
    """Semi-supervised Online Kernel Matrix Factorization.
    
    Parameters
    ----------
    
    budgetSize : int
        The size of the factorization budget.
    latentTopics : int
        The amount of latent topics.
    minibatchSize : int
        The number of elements in the minibatch
    epochs : int
        The number of epochs to train
    Gamma : double
        The initial learning rate.
    Alpha : double
        The parameter that defines the importance of the reconstruction 
        for the visual representation in feature space
    Beta : double
        The parameter that defines the importance of the reconstruction 
        for the textual representation 
    Lambda0 : double
        The parameter for the learning rate update
    Lambda1 : double
        The regularization parameter for Wv.
    Lambda2 : double
        The regularization parameter for Wt.
    Lambda3 : double
        The regularization parameter for H.
    metric : string
        The kernel metric to be used.
    kwds : 
        Optional parameters for the kernel function.
    
    Attributes
    ----------
    
    Wv : ndarray
        Budget encoding matrix for visual representation
    Wt : ndarray
        Transformation matrix from semantic to textual representation 
    H : ndarray
        Latent space.
    Budget : ndarray
        Budget matrix.
    trainErrors : list
        A list containing the reconstruction errors for initial values
        and every epoch.
    """
    def __init__(self, budgetSize, latentTopics, minibatchSize, epochs, Gamma,
                 Alpha, Beta, Lambda0, Lambda1, Lambda2, Lambda3,  metric, **kwds):
        self._budgetSize = budgetSize
        self._latentTopics = latentTopics
        self._minibatchSize = minibatchSize
        self._epochs = epochs
        self._Gamma = Gamma
        self._Alpha = Alpha
        self._Beta = Beta
        self._Lambda0 = Lambda0
        self._Lambda1 = Lambda1
        self._Lambda2 = Lambda2
        self._Lambda3 = Lambda3
        self._metric = metric
        self._kwds = kwds
        self.Wv = None
        self.Wt = None
        self.H = None
        self.Budget = None
        self._KB = None
        self._Xv = None
        self._Xt = None
        self.trainErrors = list()
        self.validationErrors = list()
        self._errorIndices = None
 
    def fit(self, Xv, Xt=None, Xvv=None, Xtv=None, calculateErrors=False, Budget=None, errorSubsetPercentage=0.1, Wv0=None, Wt0=None, initialization='random', verbose=False, shuffle=True, labeledInstances=1.0):
        """
        Train the model with a set Xv of samples.
        
        Parameters
        ----------
        
        Xv : ndarray
            A matrix with the feature representation for all the data to factorize.
        Xt : ndarray
            A matrix with label annotations.
        Xvv : ndarray
            A matrix with the feature representation to validate.
        Xvt : ndarray
            A matrix with the label representation to validata.
        calculateErrors : boolean
            If True, epoch error is calculated.
        Budget : ndarray
            Default budget
        errorSubsetPercentage: float
            percetage of the data to use in error calculation
        Wv0 : ndarra=Noney
            Initial Wv
        Wt0 : ndarray
            Initial Wt
        initialization: string ['random', 'clustering']
            The initialization method per W matrices
        """

        #print 'SSOKMF', 'V0.15'
        
        if type(labeledInstances) is float:
            self._labeledInstances = int(Xv.shape[0] * labeledInstances)
        else:
            self._labeledInstances = labeledInstances
            
        self._Xv = Xv
        self._Xt = Xt
        self._Xvnl = Xv[self._labeledInstances:,:]
        self._Xtnl = Xt[self._labeledInstances:,:]
        self._Xvl = Xv[:self._labeledInstances,:]
        self._Xtl = Xt[:self._labeledInstances,:]
            
        self._initialization = initialization
        
        if calculateErrors:
            self._errorIndices = permutation(Xv.shape[0])[:int(Xv.shape[0] * errorSubsetPercentage)]

        if not(Budget is None):
            self.Budget = Budget
        else:
            if self._budgetSize <= self._labeledInstances:
                indices = permutation(self._Xvl.shape[0])[:self._budgetSize]
                indices.sort()
                self.Budget = self._Xvl[indices,:]
            else:
                indices = permutation(self._Xvnl.shape[0])[:self._budgetSize - self._labeledInstances]
                indices.sort()
                self.Budget = np.concatenate((self._Xvl, self._Xvnl[indices,:]), axis=0)

        self._KB = K(self.Budget, None, metric=self._metric, **self._kwds)
	
        self.Wv = self._initWv() if Wv0 is None else Wv0
        if not self._Xt is None:
            self.Wt = self._initWt() if Wt0 is None else Wt0

        iteration = 0
        self.trainErrors = []
        if calculateErrors:
            if self._Xt is None:
                self.trainErrors.append(self.Error(
                        self._Xv[self._errorIndices], None))
            else:
                self.trainErrors.append(self.Error(
			self._Xv[self._errorIndices], self._Xt[self._errorIndices]))
            if Xvv != None:
                self.validationErrors.append(self.Error(Xvv, Xtv))
	t0 = time()
        for i in xrange(self._epochs):
            indices_nl = permutation(self._Xvnl.shape[0]) if shuffle else np.arange(self._Xvnl.shape[0])
            indices_l = []
            
            while len(indices_nl) > 0:
                t1 = time()
                
                if self._labeledInstances <= (self._minibatchSize / 2):
                    batchSize = min(self._minibatchSize - self._labeledInstances, len(indices_nl))
                    batch = indices_nl[:batchSize]
                    batch.sort()
                    indices_nl = indices_nl[batchSize:]
                    
                    xv = np.concatenate((self._Xvnl[batch,:], self._Xvl), axis=0)
                    if not self._Xt is None:
                        xt = np.concatenate((self._Xtnl[batch,:], self._Xtl), axis=0)
                    else:
                        xt = None
                else:
                    if len(indices_l) < (self._minibatchSize / 2):
                        if len(indices_l) == 0:
                            indices_l = permutation(self._Xvl.shape[0])
                        else:
                            indices_l = np.concatenate((indices_l, permutation(self._Xvl.shape[0]))) 
                    
                    batchSize = min((self._minibatchSize / 2), len(indices_nl))
                    batch = indices_nl[:batchSize]
                    batch.sort()
                    
                    batchSize2 = self._minibatchSize - (self._minibatchSize / 2)
                    batch_l = indices_l[:batchSize2]
                    batch_l.sort()
                    
                    indices_l = indices_l[batchSize:]
                    indices_nl = indices_nl[batchSize2:]
                    
                    xv = np.concatenate((self._Xvnl[batch,:], self._Xvl[batch_l,:]), axis=0)
                    if not self._Xt is None:
                        xt = np.concatenate((self._Xtnl[batch,:], self._Xtl[batch_l,:]), axis=0)
                    else:
                        xt = None

                self._update(xv, xt, iteration)

                iteration += 1
                if verbose:
                    t2 = time()
                    print ('{:.2%} t: {:.3} sec'.format((self._Xv.shape[0] - float(len(indices_nl))) / self._Xv.shape[0], t2 - t1))

            if calculateErrors:
                if self._Xt is None:
                    self.trainErrors.append(self.Error(
                            self._Xv[self._errorIndices], None))
                else:
                    self.trainErrors.append(self.Error(
			    self._Xv[self._errorIndices], self._Xt[self._errorIndices]))
                if Xvv != None:
                    self.validationErrors.append(self.Error(Xvv, Xtv))
        #print ('Total time: {} sec'.format(time() - t0))
        #self.H = self.predictH(self._Xv, self._Xt)
        self.trainErrors = np.array(self.trainErrors)
        self.validationErrors = np.array(self.validationErrors)
    
    def _update(self, xvi, xti, iteration):
         kxvi = self._nextKxi(xvi)
         self.H = self._nextH(kxvi, xti)
         gamma = self._nextGamma(iteration)
         self._nextWv(kxvi, gamma)
         if not xti is None:
            self._nextWt(xti, gamma)

    def _nextGamma(self, t):
        return self._Gamma / (1 + (self._Gamma * self._Lambda0 * t))
    
    def _initWv(self):
        if self._initialization == 'clustering':
            return L1Normalization(kk(self._KB, self._latentTopics, 5)[0].T)
        return np.random.rand(self._budgetSize, self._latentTopics)
        #return L1Normalization(np.random.rand(self._budgetSize, self._latentTopics))
    
    def _initWt(self):
        #if self._initialization == 'clustering':
        #    return kk(self._Xt, self._latentTopics, 5)[0].T
        return np.random.rand(self._Xt.shape[1], self._latentTopics)*0.1
        #return L1Normalization(np.random.rand(self._Xt.shape[1], self._latentTopics))
    
    def _nextKxi(self, x):
        return K(self.Budget, x, self._metric, **self._kwds)
    
    def _nextH(self, kxvi, xti=None):
        
        A = self._Alpha*dot(dot(self.Wv.T,self._KB),self.Wv)
        A += self._Lambda3*np.eye(self._latentTopics)
        if not xti is None:
            A += self._Beta*dot(self.Wt.T, self.Wt)

        b = self._Alpha*dot(self.Wv.T,kxvi)
        if not xti is None:
            b += self._Beta*dot(self.Wt.T,xti.T)
        try:
            return la.solve(A, b)
        except la.LinAlgError:
            print('Using lstsq')
            return la.lstsq(A, b)[0]
    
    def _gWv(self, kxvi):
        G = self._Alpha*dot(dot(dot(self._KB,self.Wv),self.H),self.H.T)
        G -= self._Alpha*dot(kxvi,self.H.T)
        G += self._Lambda1*self.Wv
        return G
    
    def _nextWv(self, kxvi, gamma):
        G = self._gWv(kxvi)
        self.Wv = self.Wv - gamma*G
        #self.Wv = self.Wv.clip(0)

    def _gWt(self, xti):
        G = self._Beta*dot(dot(self.Wt, self.H), self.H.T)
        G -= self._Beta*dot(xti.T, self.H.T)
        G += self._Lambda2*self.Wt
        return G

    def _nextWt(self, xt, gamma):
        G = self._gWt(xt)
        self.Wt = self.Wt - gamma*G
        #self.Wt = self.Wt.clip(0)

    def predictH(self, Xvp, Xtp=None):
        """
        Calculate the latent representation of a given data set using the
        learned model.
        
        Parameters
        ----------
        Xp : ndarray
            A matrix representing the data whose latent representation
            will be calculated.
        
        Returns
        -------
        H : ndarray
            The latent representation for the given data set.
        """
        Kxv = self._nextKxi(Xvp)
        H = self._nextH(Kxv, Xtp)
        return H


    
    def SampleLoss(self, xvi, xti=None):
        """
        Calculates the objective function for a given sample x.
        
        Parameters
        ----------
        x : ndarray
            The sample whose objective function will be calculated.
        
        Returns
        -------
        sampleError : float
            The objective function evaluated for the given sample.
        """
        E1 = np.trace(K(xvi, None, self._metric, **self._kwds))
        kxi = self._nextKxi(xvi)
        h = self._nextH(kxi, xti)

        E2 = np.trace(dot(h.T,dot(self.Wv.T,kxi)))
        E3 = np.trace(dot(dot(kxi.T,self.Wv),h))
        E4 = np.trace(dot(dot(dot(h.T,dot(self.Wv.T,self._KB)),self.Wv),h))
        vError = self._Alpha*(E1 - E2 - E3 + E4) / 2.0
   
        if xti is not None:
            E5 = xti.T - dot(self.Wt,h)
            E6 = np.trace(dot(E5.T,E5))
            tError = self._Beta*(E6) / 2.0
        else:
            tError = 0

        E7 = self._Lambda1 * np.trace(dot(self.Wv.T,self.Wv))
        if xti is not None:
            E8 = self._Lambda2 * np.trace(dot(self.Wt.T,self.Wt))
        else:
            E8 = 0
        E9 = self._Lambda3 * np.trace(dot(h.T,h))

        totalError = vError + tError + (E7 + E8 + E9) / 2.0
        return np.array([totalError, vError, tError])
    
    def Error(self, Xv, Xt):
        """
        Calculate the objective function for a given set of data using the
        factorization learned.
        
        Parameters
        ----------
        X : ndarray
            A matrix representing the data whose objective will be calculated.
        
        Returns
        -------
        error : float
            The objective function evaluated for the gven data.
        """
        error = np.array([0.0,0.0,0.0])
        indices = range(Xv.shape[0])
        while len(indices) > 0:
            batchSize = min(self._minibatchSize,len(indices))
            batch = indices[:batchSize]
            batch.sort()
            indices = indices[batchSize:]
            xv = Xv[batch,:]
            if Xt is None:
                sampleError = self.SampleLoss(xv, None)
            else:
                xt = Xt[batch,:]
                sampleError = self.SampleLoss(xv, xt)
            
            error += sampleError
        return error
