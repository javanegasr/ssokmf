# -*- coding: utf-8 -*-
"""
@author: Viviana Beltrán 
"""

import numpy as np
import numpy.linalg as la
from numpy.random import permutation
from numpy import dot
from sklearn.metrics import pairwise_kernels as K
from KKmeans import KKmeans as kk
import L1Normalization

class SSOKMF:
    """Semi-supervised Online Kernel Matrix Factorization.
    
    Parameters
    ----------
    
    budgetSize : int
        The size of the factorization budget.
    latentTopics : int
        The amount of latent topics.
    minibatchSize : int
        The number of elements in the minibatch
    epochs : int
        The number of epochs to train
    learning_rate : double
        params for the learning rate.
    current_step : double
        params for the learning rate.
    Alpha : double
        The parameter that defines the importance of the reconstruction 
        for the visual representation in feature space
    Beta : double
        The parameter that defines the importance of the reconstruction 
        for the textual representation 
    Lambda1 : double
        The regularization parameter for Wv.
    Lambda2 : double
        The regularization parameter for Wt.
    Lambda3 : double
        The regularization parameter for H.
    metric : string
        The kernel metric to be used.
    kwds : 
        Optional parameters for the kernel function.


    Attributes
    ----------
    
    Wv : ndarray
        Budget encoding matrix for visual representation
    Wt : ndarray
        Transformation matrix from semantic to textual representation 
    H : ndarray
        Latent space.
    Budget : ndarray
        Budget matrix.
    trainErrors : list
        A list containing the reconstruction errors for initial values
        and every epoch.
    """
    def __init__(self, budgetSize, latentTopics, minibatchSize, epochs,**learning_rate,
                 Alpha, Beta, Lambda1, Lambda2, Lambda3, metric,**kwds):
        self._budgetSize = budgetSize
        self._latentTopics = latentTopics
        self._minibatchSize = minibatchSize
        self._epochs = epochs
        self._learning_rate = learning_rate
        self._current_step = None
        self._Alpha = Alpha
        self._Beta = Beta
        #self._Lambda0 = Lambda0
        self._Lambda1 = Lambda1
        self._Lambda2 = Lambda2
        self._Lambda3 = Lambda3
        self._metric = metric
        self._kwds = kwds
        self.Wv = None
        self.Wt = None
        self.H = None
        self.Budget = None
        self._KB = None
        self._Xv = None
        self._Xt = None
        self.trainErrors = list()
        self.validationErrors = list()
        

'''
LEARNING_RATE_PARAMS = {
    "fixed": frozenset(["gamma"]),
    "step": (),
    "exp": (),
    "inv": frozenset(["gamma"]),
    "multistep": (),
    "poly": frozenset(["gamma"]),
    "sigmoid": frozenset(["gamma"]),
}
'''    
    def fit(self, Xv, Xt, V=None, calculateErrors=False, Budget=None):
        """
        Train the model with a set Xv of samples.
        
        Parameters
        ----------
        
        Xv : ndarray
            A matrix with the feature representation for all the data to factorize.
        Xt : ndarray
            A matrix with label annotations
        V : ndarray
            A matrix representing data to validate.
        calculateErrors : boolean
            If True, epoch error is calculated.
        Budget : ndarray
            Default budget
        """
        self._Xv = Xv
        self._Xt = Xt
  
        if not(Budget is None):
            self.Budget = Budget
        else:
            indices = permutation(self._Xv.shape[0])[:self._budgetSize]
            indices.sort()
            self.Budget = Xv[indices,:]

        self._KB = K(self.Budget, None, metric=self._metric, **self._kwds)

        self.Wv = self._initWv()
        self.Wt = self._initWt()

        iteration = 0
        if calculateErrors:
            self.trainErrors.append(self.Error(self._Xv, self._Xt))
            if V != None:
                self.validationErrors.append(self.Error(V))

        for i in xrange(self._epochs):
            indices = permutation(self._Xv.shape[0])
            while len(indices) > 0:
                batchSize = min(self._minibatchSize, len(indices))
                batch = indices[:batchSize]
                batch.sort()
                indices = indices[batchSize:]
                xv = self._Xv[batch,:]
                xt = self._Xt[batch,:]

                self._update(xv, xt, iteration)

                iteration += 1
            if calculateErrors:
                self.trainErrors.append(self.Error(self._Xv, self._Xt))
                if V != None:
                    self.validationErrors.append(self.Error(V))
        self.H = self.predictH(self._Xv, self._Xt)
        self.trainErrors = np.array(self.trainErrors)
        self.validationErrors = np.array(self.validationErrors)
    
    def _update(self, xvi, xti, iteration):
         kxvi = self._nextKxi(xvi)
         self.H = self._nextH(kxvi, xti)
         gamma = self._nextGamma(iteration)
         self._nextWv(kxvi, gamma)
         self._nextWt(xti, gamma)

    '''
    // Return the current learning rate. The currently implemented learning rate
    // policies are as follows:
    //    - fixed: always return base_lr.
    //    - step: return base_lr * gamma ^ (floor(iter / step))
    //    - exp: return base_lr * gamma ^ iter
    //    - inv: return base_lr * (1 + gamma * iter) ^ (- power)
    //    - multistep: similar to step but it allows non uniform steps defined by
    //      stepvalue
    //    - poly: the effective learning rate follows a polynomial decay, to be
    //      zero by the max_iter. return base_lr (1 - iter/max_iter) ^ (power)
    //    - sigmoid: the effective learning rate follows a sigmod decay
    //      return base_lr ( 1/(1 + exp(-gamma * (iter - stepsize))))
    // 
    // where base_lr, max_iter, gamma, step, stepvalue and power are defined
    // init function, and iter is the current iteration.    
    '''
    def _nextGamma(self, t):
        policy = self._learning_rate.get('policy')
        gamma = self._learning_rate.get('gamma')
        if policy == 'fixed':
            rate = gamma
        elif policy == 'step':
            self._current_step = t/self._learning_rate.get('stepsize')
            rate = gamma*np.power(gamma,self._current_step)
        elif policy == 'exp':
            rate = gamma*np.power(gamma,t)
        elif policy == 'inv':
            rate = gamma* np.power(1+self._learning_rate['lambda0']*t,-1*self._learning_rate['power']) 
        elif policy == 'multistep':
            rate =
        elif policy == 'poly':
            rate = gamma*np.power(1-(t/self._learning_rate['max_iter'])*1.0,self._learning_rate['power'])
        elif policy == 'sigmoid':
            rate = gamma*(1/(1+np.exp(-1*self._learning_rate['lambda0']*t-self._learning_rate['stepsize'])))
        #return self._Gamma / (1 + (self._Gamma * self._Lambda0 * t))
        else :
            LOG(FATAL) << "Unknown learning rate policy: " << lr_policy;
        return rate;

    
    def _initWv(self):
        return np.random.rand(self._budgetSize, self._latentTopics)
    
    def _initWt(self):
        return np.random.rand(self._Xt.shape[1], self._latentTopics)
    
    def _nextKxi(self, x):
        return K(self.Budget, x, self._metric, **self._kwds)
    
    def _nextH(self, kxvi, xti=None):
        A = self._Alpha*dot(dot(self.Wv.T,self._KB),self.Wv)
        A += self._Lambda3*np.eye(self._latentTopics)
        A += self._Beta*dot(self.Wt.T, self.Wt)
         
        b = self._Alpha*dot(self.Wv.T,kxvi)
        if not(xti is None):
            b += self._Beta*dot(self.Wt.T,xti.T)

        try:
            return la.solve(A, b)
        except la.LinAlgError:
            print('Using lstsq')
            return la.lstsq(A, b)[0]
    
    def _gWv(self, kxvi):
        G = self._Alpha*dot(dot(dot(self._KB,self.Wv),self.H),self.H.T)
        G -= self._Alpha*dot(kxvi,self.H.T)
        G += self._Lambda1*self.Wv
        return G
    
    def _nextWv(self, kxvi, gamma):
        G = self._gWv(kxvi)
        self.Wv = self.Wv - gamma*G
        #self.Wv = self.Wv.clip(0)

    def _gWt(self, xti):
        G = self._Beta*dot(dot(self.Wt, self.H), self.H.T)
        G -= self._Beta*dot(xti.T, self.H.T)
        G += self._Lambda2*self.Wt
        return G

    def _nextWt(self, xt, gamma):
        G = self._gWt(xt)
        self.Wt = self.Wt - gamma*G
        #self.Wt = self.Wt.clip(0)

    def predictH(self, Xvp, Xtp=None):
        """
        Calculate the latent representation of a given data set using the
        learned model.
        
        Parameters
        ----------
        Xp : ndarray
            A matrix representing the data whose latent representation
            will be calculated.
        
        Returns
        -------
        H : ndarray
            The latent representation for the given data set.
        """
        Kxv = self._nextKxi(Xvp)
        H = self._nextH(Kxv, Xtp)
        return H


    
    def SampleLoss(self,xvi, xti):
        """
        Calculates the objective function for a given sample x.
        
        Parameters
        ----------
        x : ndarray
            The sample whose objective function will be calculated.
        
        Returns
        -------
        sampleError : float
            The objective function evaluated for the given sample.
        """
        E1 = np.trace(K(xvi, None, self._metric, **self._kwds))
        kxi = self._nextKxi(xvi)
        h = self._nextH(kxi)
        E2 = np.trace(dot(h.T,dot(self.Wv.T,kxi)))
        E3 = np.trace(dot(dot(kxi.T,self.Wv),h))
        E4 = np.trace(dot(dot(dot(h.T,dot(self.Wv.T,self._KB)),self.Wv),h))
        vError = self._Alpha*(E1 - E2 - E3 + E4) / 2.0
   
        E5 = xti.T - dot(self.Wt,h)
        E6 = np.trace(dot(E5.T,E5))
        tError = self._Beta*(E6) / 2.0

        E7 = self._Lambda1 * np.trace(dot(self.Wv.T,self.Wv))
        E8 = self._Lambda2 * np.trace(dot(self.Wt.T,self.Wt))
        E9 = self._Lambda3 * np.trace(dot(h.T,h))

        totalError = vError + tError + (E7 + E8 + E9) / 2.0
        return np.array([totalError, vError, tError])
    
    def Error(self, Xv, Xt):
        """
        Calculate the objective function for a given set of data using the
        factorization learned.
        
        Parameters
        ----------
        X : ndarray
            A matrix representing the data whose objective will be calculated.
        
        Returns
        -------
        error : float
            The objective function evaluated for the gven data.
        """
        error = np.array([0.0,0.0,0.0])
        indices = range(Xv.shape[0])
        while len(indices) > 0:
            batchSize = min(self._minibatchSize,len(indices))
            batch = indices[:batchSize]
            batch.sort()
            indices = indices[batchSize:]
            xv = Xv[batch,:]
            xt = Xt[batch,:]
            sampleError = self.SampleLoss(xv, xt)
            error += sampleError
        return error
